# -*- coding: utf-8 -*-
"""
Created on Wed Oct 10 18:51:38 2018
 
@author: Miguel & Diana
"""
import matplotlib.pyplot as plt
import numpy as np
from sklearn.utils import shuffle
from sklearn.model_selection import StratifiedKFold
from sklearn.cross_validation import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors.kde import KernelDensity
from sklearn.metrics import accuracy_score 

def calc_fold(X, Y, train_ix, test_ix, c)   :
    ''' return the training and test error'''
    #use C = 1 and double for regularization of logistic regression
    reg = LogisticRegression(penalty='l2',C=c, tol=1e-10)
    reg.fit(X[train_ix, :], Y[train_ix].ravel())
   
    #Use the fraction of incorrect classifications as the measure of the error.
    train_error = 1 - reg.score(X[train_ix, :], Y[train_ix])
    test_error = 1 - reg.score(X[test_ix, :], Y[test_ix])
    return (train_error, test_error)
   
    prob= reg.predict_proba(X[:,:])[:,1]
    squares = (prob-Y[:,0])**2
    return (np.mean(squares[train_ix]), np.mean(squares[test_ix]))
       
def calc_knn(k, X, Y, train_ix, valid_ix) :
    model = KNeighborsClassifier(n_neighbors=k)
    model.fit(X[train_ix, :], Y[train_ix].ravel())
    err_tr = 1 - model.score(X[train_ix, :], Y[train_ix])
    err_val= 1 - model.score(X[valid_ix, :], Y[valid_ix])
     #can't use test error!! it will be biased 
     #fraction of incorrect classification
    
    #training error = média dos training errors dos folds usados n_splits-1 = 4
    #cross validation error = usando o fold que fica de fora
    return (err_tr, err_val)


def split_by_classes(X_r, Y_r):
    Y_r = Y_r.ravel()
    
    X_r_0 = X_r[Y_r==0] 
    X_r_1 = X_r[Y_r==1]
    Y_r_0 = Y_r[Y_r==0]
    Y_r_1 = Y_r[Y_r==1]
    
    #array_by_class = []
    #real = [] #zero
    #fake = [] #um
    #real.append(X_r_0)
    #real.append(Y_r_0)
    
    #fake.append(X_r_1)
    #fake.append(Y_r_1)

    #array_by_class.append(real)
    #array_by_class.append(fake)
    #return array_by_class
    return X_r_0, X_r_1, Y_r_0, Y_r_1

def bayes_fit(X_r, Y_r, bw, features):
     #split data by classes
     
    X_r_0, X_r_1, Y_r_0, Y_r_1 = split_by_classes (X_r, Y_r)
    
    array_kde_0 = []
    array_kde_1 = []
    
    #for each class calculate kernel density and bayes
    for feats in range(0, features):
        #calculate different kde for each feature by class
        kde_0 = KernelDensity(kernel='gaussian', bandwidth=bw)
        kde_1 = KernelDensity(kernel='gaussian', bandwidth=bw)
        kde_0.fit(X_r_0[:, [feats]])
        kde_1.fit(X_r_1[:, [feats]])
        array_kde_0.append(kde_0)
        array_kde_1.append(kde_1)
        
    #probability of real
    pb_0 = np.log(float(len(X_r_0)/len(X_r)))
    
    #probability of fake
    pb_1 = np.log(float(len(X_r_1)/len(X_r)))
        
    return array_kde_0, array_kde_1, pb_0, pb_1
    
def bayes_classifier(kde_0, kde_1, pb_0, pb_1, X_r, features):
    #score of kde to get probabilities
    new_kde_0 = [None] * features
    new_kde_1 = [None] * features
    
    #classify into the classes
    classified_data = np.zeros(len(X_r))
    
    for feats in range(features):
        new_kde_0[feats] = kde_0[feats].score_samples(X_r[:, [feats]])
        new_kde_1[feats] = kde_1[feats].score_samples(X_r[:, [feats]])
    
    #classify in classes based on sum of features
    for l in range(len(X_r)):
        real = 0
        fake = 0
        #probabilidade condicionada das features pelas classes
        for feats in range(features):
            real = real + new_kde_0[feats][l]
            fake = fake + new_kde_1[feats][l]
        
        #soma da probabilidade de classes com a probabilidade condicionada
        real = real + pb_0
        fake = fake + pb_1
        if(real < fake):
            classified_data[l] = 1
    return classified_data

def calc_bayes(X_r, Y_r, tr_ix, va_ix, bw, feats):
   kde_0, kde_1, pb_0, pb_1 = bayes_fit(X_r[tr_ix, :], Y_r[tr_ix], bw, feats)
   pred_train = bayes_classifier(kde_0, kde_1, pb_0, pb_1, X_r[tr_ix,:], feats)
   pred_valid = bayes_classifier(kde_0, kde_1, pb_0, pb_1, X_r[va_ix,:], feats)
   return (1 - accuracy_score(Y_r[tr_ix], pred_train), (1 - accuracy_score(Y_r[va_ix], pred_valid)))     
        

def draw_plot(paramChanging, arrayTrain, arrayValid, title):
    plt.figure(1, figsize=(12, 8))
    plt.plot(paramChanging,arrayTrain, '--c',label='Training Error')
    plt.plot(paramChanging,arrayValid, label='Validation Error')
    plt.legend(loc='lower right')
    plt.title(title)
    plt.show()
    plt.close()
  
def mcNemars(Ys, test_error_c, test_error_k, test_error_bw):
    err_c = np.zeros(len(Ys))
    err_k = np.zeros(len(Ys))
    err_bw = np.zeros(len(Ys))
    count1=0
    count2=0
    count3=0
    #check for the errors against the test data
    for i in range(len(Ys)):
        if(test_error_c[i] != Ys[i]):
            err_c[i] = 1
        if(test_error_k[i] != Ys[i]):
            err_k[i] = 1
        if(test_error_bw[i] != Ys[i]):
            err_bw[i] = 1
    e01, e10 = compare(err_c, err_k)
    res_c_k = mcNemarsFormula(e01, e10)
   
    e01, e10 = compare(err_c, err_bw)
    res_c_bw = mcNemarsFormula(e01, e10)
     
    e01, e10 = compare(err_k, err_bw)
    res_k_bw = mcNemarsFormula(e01, e10)
   
    return res_c_k, res_c_bw, res_k_bw
         
def compare(class1, class2):
    e01=e10=0
    for i in range(len(class1)):
        #e01 be the number of examples the first classifier misclassifies but the second classifies correctly
        if (class1[i]==1 and class2[i]==0):
            
            e01=e01+1
        #e10 be the number of examples the second classifier classifies incorrectly but the first classifier classifies correctly.
        if (class1[i]==0 and class2[i]==1):
            e10=e10+1
    
    return e01,e10

def mcNemarsFormula(e01, e10):
    result =float(float((np.abs(e01-e10)-1)**2)/float(e01+e10))       
    return result

#CONSTANTES

#K-Fold 5 folds
folds=5
features = 4
      
#LOAD DATA
data=np.loadtxt("TP1-data.csv",delimiter=",")

#SHUFFLE
mat= shuffle(data)


Xs=mat[:,:-1]
Ys=mat[:,[-1]]

#STANDARDIZATION
means= np.mean(Xs,0)
stdevs=np.std(Xs,0)
Xs=(Xs-means)/stdevs

#TRAINING + CROSS VALIDATION:

#STRATIFIED SPLIT
#USE 2/3 FOR TRAIN AND 1/3 FOR TESTING
#X_r - X_train e X_t é X_test
X_r,X_t,Y_r,Y_t = train_test_split(Xs, Ys, test_size=0.33, stratify = Ys)

#USE 5 FOLDS FOR CROSS VALIDATION
kf = StratifiedKFold(n_splits=folds)

#Logistic Regression

paramChanging=[]
arrayTrain=[]
arrayValid=[]
best_c = 0
best_err = 10000000 # very large number
c=1
#cycle of i to 20 for c variation
for i in range(1, 20):
    tr_err=va_err=med_train_log=med_val_log=0
    for tr_ix, va_ix in kf.split(X_r,Y_r):
        r,v = calc_fold(X_r,Y_r,tr_ix,va_ix, c)
        tr_err += r
        va_err += v
    

    c = 2*c
    
    med_train_log = float(tr_err/folds)
    med_val_log = float(va_err/folds)
    
    if med_val_log < best_err:
        best_err = med_val_log
        best_c = np.log2(c)
        
    #plot errors againt the log of c
    paramChanging.append(np.log2(c))
    arrayTrain.append(med_train_log)
    arrayValid.append(med_val_log)  

title = 'Training and Validation Error for Logistic Regression'
draw_plot(paramChanging, arrayTrain, arrayValid, title)

#KNN
paramChangingK=[]
arrayValidK=[]
arrayTrainK=[]

best_k = 0
best_err_k = 10000000
#use k values between 1 and 39, with only odd numbers
for k in range(1, 40,2):
    #Create KNN Classifier
    tr_err=va_err=med_train_k=med_val_k=0  
    for tr_ix, va_ix in  kf.split(X_r,Y_r):
        #Train the model using the training sets
        r,v = calc_knn(k, X_r, Y_r, tr_ix, va_ix)
        tr_err += r
        va_err += v
        
    #medians     
    med_train_k = float(tr_err/folds)
    med_val_k = float(va_err/folds)
    if med_val_k < best_err_k:
        
        best_err_k = med_val_k
        best_k = k
        
        
    paramChangingK.append(k)
    arrayTrainK.append(med_train_k)
    arrayValidK.append(med_val_k)  

   
title = 'Training and Validation Errors for K-NNeighbour'
draw_plot(paramChangingK, arrayTrainK, arrayValidK, title)


print("""We are building things simply amazing.
      Please be patient... It will take a while...""")
#BAYES

best_bw = 0
best_err_bw = 10000000

paramChangingBW=[]
arrayValidBW=[]
arrayTrainBW=[]
#bandwith changes from 0.01 to 1, with step 2
for bw in np.arange(0.01, 1, 0.02):

    tr_err=va_err=med_val_bw=med_train_bw=0  
    for tr_ix, va_ix in  kf.split(X_r,Y_r):
        r,v =calc_bayes(X_r, Y_r, tr_ix, va_ix, bw, features)
        tr_err += r
        va_err += v
            
    #medians
    med_train_bw = float(tr_err/folds)
    med_val_bw = float(va_err/folds)
    if med_val_bw < best_err_bw:
        best_err_bw = med_val_bw
        best_bw = bw
        
    paramChangingBW.append(bw)
    arrayTrainBW.append(med_train_bw)
    arrayValidBW.append(med_val_bw)  
   
title = 'Training and Validation Errors for Naive Bayes classifier.'
draw_plot(paramChangingBW, arrayTrainBW, arrayValidBW, title)
     
        

                

#TESTING: True Test Error
print("Testing True Error:\n")
#Logistic Regression
reg = LogisticRegression(penalty='l2',C=best_c, tol=1e-10)
reg.fit(X_r,Y_r.ravel())
test_error_c = reg.predict(X_t)
print("Logistic Regression:")
print("BEST C: ", best_c, "\n")

#Knn
knn = KNeighborsClassifier(best_k)
knn.fit(X_r, Y_r.ravel())
test_error_k = knn.predict(X_t)
print("k-NNeighbours:")
print("BEST K: ", best_k, "\n")

#Bayes

#bayes_fit(X_r, Y_r, best_bw)
#bayes_pred(X_t)
real_kde, fake_kde, real_pb, fake_pb = bayes_fit(X_r, Y_r, best_bw, features)
test_error_bw = bayes_classifier(real_kde, fake_kde, real_pb, fake_pb, X_t, features)
print("Naive Bayes:")
print("Best BW: ", best_bw, "\n")

#McNemar's COMPARITION WITH 95% of confidence
#Ys? faz me sentido comparar os valor
logReg_vs_KNN, KNN_vs_NB,NB_vs_LogReg=mcNemars(Y_t, test_error_c, test_error_k, test_error_bw)
print("Mcnemar's Test:\n")
print("logReg_vs_KNN: ", logReg_vs_KNN)
print("KNN_vs_NB: ", KNN_vs_NB)
print("NB_vs_LogReg",NB_vs_LogReg)
