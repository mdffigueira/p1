# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 03:19:16 2018

@author: migue
"""

def shuffle_data(data):
    ranks = np.arange(data.shape[0])
    np.random.shuffle(ranks)
    mat=data[ranks,:]
    return mat
